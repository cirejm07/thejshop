import React from 'react';
import HomeBanner from '../components/HomeBanner'
import {AnimatePresence, motion} from 'framer-motion'
export default function  Home() {

let samepleProp = {
			title: "Dota 2 Accessories and more!",
			description: "“Feel the Quality”.",
			buttonCallToAction: "Buy now",
			destination: "/products"
		};
	return(
		<motion.div initial={{ scaleY:0 }} animate={{ scaleY:1 }} exit={{ scaleY: 0 }} transition={{duration: 0.5}}>

			<HomeBanner bannerProp={samepleProp}/>
		</motion.div>

		)

}