import React,{useState,useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {AnimatePresence, motion} from 'framer-motion'
// import react router dom components for simulated page router
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';

// import our user provider - provider component of our context
import {UserProvider} from './userContext';

// import components
import HomeBanner from './components/HomeBanner';
import AppNavBar from './components/AppNavBar';
import NotFound from './components/NotFound'
import Product from './components/Product';

// import pages
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AddProduct from './pages/AddProduct'
import ViewProduct from './pages/viewProducts'
import Products from './pages/Products'
import Cart from './pages/Cart'
import UpdateProduct from './pages/UpdateProduct'
// css
import './App.css'



export default function App(){

const [user,setUser] = useState({
  id: null,
  isAdmin: null,
})

// useEffect to fetch our user's detail
useEffect(() => {

  fetch('https://secure-cove-16853.herokuapp.com/users/getUserDetails',{
    headers:{
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
    setUser({

      id: data._id,
      isAdmin: data.isAdmin
    })
  })

},[])

const unsetUser = () => {
    //clears the content of our localStore
    localStorage.clear()
  }



  return (
    <>
    <UserProvider value ={{user,setUser,unsetUser}}>
      <Router>
       <AppNavBar />
        <Container>
        <AnimatePresence exitBeforeEnter>
          <Switch>
            <Route exact path="/" component ={Home}/>
            <Route exact path="/register" component ={Register}/>
            <Route exact path="/login" component ={Login}/>
            <Route exact path="/logout" component ={Logout}/>
            <Route exact path="/addProduct" component ={AddProduct}/>
            <Route exact path="/products" component ={Products}/>
            <Route exact path="/products/:productId" component ={ViewProduct}/>
            <Route exact path="/cart" component ={Cart}/>
            <Route exact path="/updateproduct/:productId" component ={UpdateProduct}/>
            <Route component ={NotFound}/>
          </Switch> 
          </AnimatePresence>
        </Container>
      </Router>
    </UserProvider>       
    </>
    )
}