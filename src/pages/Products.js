import React,{useState,useEffect,useContext} from 'react';

import {Redirect,useHistory} from 'react-router-dom';
import Product from '../components/Product'
import {Link} from 'react-router-dom';
import UserContext from '../userContext';

import {Table,Button,Nav} from 'react-bootstrap'

import '../App.css'

export default function Products() {
	const history = useHistory();
	const{user} = useContext(UserContext);
	console.log(user)

	// create a state with empty array as initial value
	const [productsArray,setProductsArray] = useState([]);
	const [allProducts,setAllProducts] = useState([]);
	const [update,setUpdate] = useState(0);



useEffect(()=>{

	fetch('https://secure-cove-16853.herokuapp.com/products/retrieveActiveProduct')
	.then(res => res.json())
	.then(data =>{
		console.log(data)
		setProductsArray(data.map(product => {
		return (
			<>

			<Product key={product._id} productProp={product}/>

			</>
			) 

		}))

	})

},[])


	useEffect(() => {

			if(user.isAdmin) {
				fetch('https://secure-cove-16853.herokuapp.com/products/',{
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					setAllProducts(data.map((product =>{

						return(

							<tr key={product._id}>
								<td>{product._id}</td>
								<td>{product.name}</td>
								<td>{product.price}</td>
								<td>{product.isActive ? "Active": "Inactive"}</td>
								<td>{product.createdOn}</td>
								<td>
								{
										product.isActive
										?
											<Button variant="danger" className="mx-2" onClick={()=>{

												archive(product._id)
											}}>Archive</Button>
										: 
										<Button variant="success" className="mx-2" onClick={()=>{
											activate(product._id)
										}}>Activate</Button>
								}
								</td>
								<td>
								<Button variant="info" className="mx-2" onClick={()=>{
										updateProduct(product._id)
									}}>Update</Button>
								</td>

							</tr>
						
						

						)

					})))
				})
			}

	},[user,update])

	function updateProduct(productId) {
		console.log(productId);
		history.push(`/updateproduct/${productId}`);
	}


	function archive (productId) {

		console.log(productId)
		fetch(`https://secure-cove-16853.herokuapp.com/products/archiveProduct/${productId}`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// re-render by updating a state
			setUpdate(update+1)
		})
	}

	function activate (productId) {

		console.log(productId)
		fetch(`https://secure-cove-16853.herokuapp.com/products/activeProduct/${productId}`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// re-render by updating a state
			setUpdate(update+1)
		})
	}



	return(
		user.isAdmin
		?
		<>

			<h1 className="text-center my-5">Product Dashboard</h1>
			<Link to="/addProduct"  className="addProduct btn btn-primary">Add Product</Link>
			<Table stripped bordered hover>
				<thead>
					<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Price</th>
					<th>Status</th>
					<th>Created On</th>
					<th>Actions</th>
					<th>Update</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Product Id</td>
						<td>Product Name</td>
						<td>Product Price</td>
						<td>Product Status</td>
						<td>Date Created</td>
						<td>Archive/Active</td>
						<td>Update Product</td>
					</tr>
					{allProducts}
				</tbody>
			</Table>
		</>
		:
		<>
			<h1 className="my-5 text-center">Available Product</h1>
			<div>{productsArray}</div>
		</>
		)

}