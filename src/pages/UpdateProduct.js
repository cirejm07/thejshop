import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Redirect,useHistory,useParams} from 'react-router-dom';
import UserContext from '../userContext'

export default function UpdateProduct(){


	const {user} = useContext(UserContext);
	// console.log(user);

	const history = useHistory();

	const productId = useParams().productId;
	// console.log(productId);

	const [name,setName] = useState("")
	const [description,setDescription] = useState("")
	const [price,setPrice] = useState(0)

	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{

		// form validation
		if((name !== "" && description !== "" && price !== 0) && (price > 0)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[name,description,price])

	useEffect(()=>{

		fetch(`https://secure-cove-16853.herokuapp.com/products/retrieveSingleProduct/${productId}`)
		.then(res=>res.json())
		.then(data=> {

			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Product Unavailable",
					text: data.message
				})

			} else {
				
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				
			}

		})

	},[])

	function updateProduct(e){

		e.preventDefault();

		console.log(name);
		console.log(description);
		console.log(price);

		console.log(productId);

		fetch(`https://secure-cove-16853.herokuapp.com/products/updateProduct/${productId}`,{

			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price

			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data.name){
				const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Are you sure?',
  text: `The product update can't be reverted.`,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Update',
  cancelButtonText: 'Cancel',
  reverseButtons: true
}).then((result) => {
  if (result.isConfirmed) {
    swalWithBootstrapButtons.fire(
      'Updated!',
      'Your Product has been updated!',
      'success'
    )
    history.push('./login')
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Cancelled Update',
      'error'
    )
  }
})
			
		  }	else {

				Swal.fire({
					
					icon: "error",
					title: "Update Failed.",
					text: data.message
				})
			}

		})

	}

	return (

		user.isAdmin
		?
		<>
			<h1 className="my-5 text-center">Update Product</h1>
			<Form onSubmit={e => updateProduct(e)}>
				<Form.Group>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder={name} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder={description} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price (PHP):</Form.Label>
					<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}} placeholder={price} required/>
				</Form.Group>
				{
					isActive
					? <Button variant="warning" type="submit">Save</Button>
					: <Button variant="warning" disabled>Save</Button>
				}
			</Form>
		</>
		:
		<Redirect to="/login"/>

	)


}
