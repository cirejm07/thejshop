import React from 'react'
import '../App.css'
import HomeBanner from './HomeBanner'
export default function NotFound() {
let samepleProp2 = {
			title: "OOOOOPS? Wrong page mate?",
			description: "“Lost in the woods, are you?” — Luna.",
			buttonCallToAction: "Get back to page.",
			destination: "/"
		};
	return (
		<HomeBanner bannerProp={samepleProp2}/>

		)
}
