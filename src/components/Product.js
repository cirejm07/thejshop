import React,{useState,useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../App.css'
export default function Product({productProp}){

	return(
		<>
		
	<Card className="col-md-4 bg-transparent d-inline-flex align-items-stretch setCard  border">
		<Card.Body className="border">
			
			<Card.Img variant="top" src="https://media3.giphy.com/media/8BlByFsU4FZJ41AJpd/giphy.gif?cid=ecf05e473y8p50wnvl2v1b1if8bbs45upojq4v56nup4zxav&rid=giphy.gif&ct=s" />
			<Card.Title>{productProp.name}</Card.Title>
			<Card.Subtitle>Description:</Card.Subtitle>
			<Card.Text>{productProp.description}:</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>PHP {productProp.price}</Card.Text>

		
			<Link className="btn btn-primary" to={`/products/${productProp._id}`}>View Product</Link>

		</Card.Body>

	</Card>
</>
		)
}

